#database.py
#Defines a class to manipulate hard data on the SQLite3 db
from sqlalchemy import create_engine, MetaData, Table, between
from sqlalchemy.engine.url import URL
from datetime import date, timedelta
import pandas as pd
import dataget
class Database():
    def __init__( self ) :
        self.DATABASE_FILENAME = 'povespa-special-loadgoog.sqlite3'
        self.DRIVER_NAME = 'sqlite'
        self.TABLE_NAME = 'stocks'
        self.COLUMNS_NAMES = ['Date','Symbol','Open','High','Low','Close','Adj Close','Volume']
        self.engine = None
        self.DATABASE = {
            'drivername': self.DRIVER_NAME,
            # 'host': 'localhost',
            # 'port': '5432',
            # 'username': 'YOUR_USERNAME',
            # 'password': 'YOUR_PASSWORD',
            'database': self.DATABASE_FILENAME
        }

    def our_engine(self):
        if(self.engine is None):
            self.engine = create_engine(URL(**self.DATABASE), echo=False)
        return self.engine

    def update():
        db = Database()
        tables = db.get_tables_available()
        for s in tables['name']:
            if('.SA' in s):
                db.update_table(s)
        return 0

    def update_table(self, s):
        con = self.our_engine()
        yesterday = date.today().strftime('%Y-%m-%d')
        sql_data = self.load_dataframe_from_sql(table_name=s)
        try:
            sql_max = pd.Timestamp(sql_data.index.max()).__format__('%Y-%m-%d')
        except:
            print(s + ' is buggedddd')
            return
        if(yesterday <= sql_max):
            print(s + ' max sql date equals today s date, not updating')
            return
        web_data = dataget.get_data(s)
        if(not web_data.empty):
            meta = MetaData()
            current_table = Table(s, meta, autoload=True, autoload_with=con)
            web_min = web_data.index.min().__format__('%Y-%m-%d')
            web_max = web_data.index.max().__format__('%Y-%m-%d')
            delete = current_table.delete().where(between(current_table._columns.Date, web_min, web_max))
            con.execute(delete)
            self.save_dataframe_to_sql(df=web_data, table_name=s)
        
    def save_dataframe_to_sql(self, df, table_name):
        try:
            df.to_sql(table_name, con=self.our_engine(), if_exists='replace')
        except Exception as e:
            print("Dupe data: {}".format(e))    

    def get_tables_available(self, con = None):
        if (con is None):
            con = self.our_engine()
        sql =  "SELECT name FROM sqlite_master WHERE type='table' ORDER BY name;"
        dados = pd.read_sql_query(sql=sql, con=con)        
        return dados

    def load_dataframe_from_sql(self, empresa = '', con=None, table_name=None, limit=None):
        if (con is None):
            con = self.our_engine()
        if (table_name is None):
            table_name = self.TABLE_NAME

        if(limit is not None):
            sql = 'select * from stocks limit '+str(limit)+';'
            dados = pd.read_sql_query(sql=sql, con=con,index_col='Date')
        elif (empresa != ''):
            sql = 'SELECT Date, Symbol, "Open", High, Low, "Close", "Adj Close", Volume FROM stocks where Symbol = "'+empresa+'";'
            dados = pd.read_sql_query(sql=sql, con=con,index_col='Date')
        else:
            dados = pd.read_sql_table(table_name=table_name, con=con, index_col='Date', columns=self.COLUMNS_NAMES)
        return dados

    def get_df_store(self):
        new_all_df = dict()
        with pd.HDFStore('df_store_blosc-zstd.h5',complevel=9, complib='blosc:zstd') as df_store:
            for i in df_store.keys():
                new_all_df[i] = df_store[i]

            df_store.close()
        return new_all_df
#result = Database.update()
