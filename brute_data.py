import database
import indicators

#@title brute data
def get_all():
    db = database.Database()
    """
    empresas_bovespa = ['ABEV3.SA','AZUL4.SA','B3SA3.SA','BBAS3.SA','BBDC3.SA','BBDC4.SA','BBSE3.SA','BEEF3.SA','BPAC11.SA','BRAP4.SA',
                        'BRDT3.SA','BRFS3.SA','BRKM5.SA','BRML3.SA','BTOW3.SA','CCRO3.SA','CIEL3.SA','CMIG4.SA','COGN3.SA','CPFE3.SA',
                        'CRFB3.SA','CSAN3.SA','CSNA3.SA','CVCB3.SA','CYRE3.SA','ECOR3.SA','EGIE3.SA','ELET3.SA','ELET6.SA','EMBR3.SA',
                        'ENBR3.SA','ENGI11.SA','EQTL3.SA','EZTC3.SA','FLRY3.SA','GGBR4.SA','GNDI3.SA','GOAU4.SA','GOLL4.SA','HAPV3.SA',
                        'HGTX3.SA','HYPE3.SA','IGTA3.SA','IRBR3.SA','ITSA4.SA','ITUB4.SA','JBSS3.SA','KLBN11.SA','LAME4.SA','LREN3.SA',
                        'MGLU3.SA','MRFG3.SA','MRVE3.SA','MULT3.SA','NTCO3.SA','PETR3.SA','PETR4.SA','PRIO3.SA','QUAL3.SA',
                        'RADL3.SA','RAIL3.SA','RENT3.SA','SANB11.SA','SBSP3.SA','SULA11.SA','SUZB3.SA','TAEE11.SA','TOTS3.SA',
                        'UGPA3.SA','USIM5.SA','VALE3.SA','VIVT4.SA','VVAR3.SA','WEGE3.SA','YDUQ3.SA'
                        ]
    """
    #PCAR : info muito ruin
    #'TIMS3.SA' problemas na database
    empresas_bovespa = db.get_tables_available()['name']
    brute_df = dict()

    for i in empresas_bovespa:
        if(i != 'stocks'):
            try:
                df = db.load_dataframe_from_sql(table_name=i)
                df = indicators.fix_columns_to_float(df)        
                brute_df[i] = df
            except:
                print(i)
                print('failrure')
    """
    brute_df['ABEV3.SA'] = brute_df['ABEV3.SA'].loc['2006-12-01':]
    brute_df['BRAP4.SA'] = brute_df['BRAP4.SA'].loc['2000-12-01':]
    brute_df['BRFS3.SA'] = brute_df['BRFS3.SA'].loc['2000-08-01':]
    brute_df['BRKM5.SA'] = brute_df['BRKM5.SA'].loc['2003-11-01':]
    brute_df['CMIG4.SA'] = brute_df['CMIG4.SA'].loc['2006-06-01':]
    brute_df['CPFE3.SA'] = brute_df['CPFE3.SA'].loc['2006-02-01':]
    brute_df['CYRE3.SA'] = brute_df['CYRE3.SA'].loc['2005-01-01':]
    brute_df['ENBR3.SA'] = brute_df['ENBR3.SA'].loc['2005-03-01':]
    brute_df['EQTL3.SA'] = brute_df['EQTL3.SA'].loc['2010-01-01':]
    brute_df['GOAU4.SA'] = brute_df['GOAU4.SA'].loc['2003-01-01':]
    brute_df['HGTX3.SA'] = brute_df['HGTX3.SA'].loc['2017-09-01':]
    brute_df['LAME4.SA'] = brute_df['LAME4.SA'].loc['2007-09-01':]
    brute_df['LREN3.SA'] = brute_df['LREN3.SA'].loc['2005-09-01':]
    brute_df['SBSP3.SA'] = brute_df['SBSP3.SA'].loc['2007-06-01':]
    brute_df['SUZB3.SA'] = brute_df['SUZB3.SA'].loc['2018-01-01':]
    brute_df['UGPA3.SA'] = brute_df['UGPA3.SA'].loc['2012-01-01':]
    brute_df['PRIO3.SA'] = brute_df['PRIO3.SA'].loc['2012-06-01':]
    """
    print('Done')
    return brute_df