import database
import indicators

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib as mpl

import ta

db = database.Database()
df = db.load_dataframe_from_sql(table_name='BBAS3.SA')
df = indicators.fix_columns_to_float(df)
# #need to fix DF values
# indicators.add_indicator_to_df(df, 'MA', 5)
# indicators.add_indicator_to_df(df, 'MA', 10)
# indicators.add_indicator_to_df(df, 'MA', 30)
# indicators.add_indicator_to_df(df, 'MACD')
df['plr'] = indicators.add_label(df)
df['ta_rsi'] = ta.momentum.RSIIndicator(close=df['Close'],fillna=False).rsi()
df['ta_wr'] = ta.momentum.WilliamsRIndicator(df['High'],df['Low'],df['Close'],fillna=False).wr()
df['ta_cci'] = ta.trend.CCIIndicator(df['High'],df['Low'],df['Close'],fillna=False).cci()
df['ta_macd'] = ta.trend.MACD(close=df['Close'],n_slow=15,n_fast=5,n_sign=21,fillna=False).macd()
df['+adx'] = ta.trend.ADXIndicator(df['High'],df['Low'],df['Close'],fillna=False).adx_pos()
df['-adx'] = ta.trend.ADXIndicator(df['High'],df['Low'],df['Close'],fillna=False).adx_neg()
df['adx'] = ta.trend.ADXIndicator(df['High'],df['Low'],df['Close'],fillna=False).adx()
df['adxr_14'] = ta.trend.ADXIndicator(df['High'],df['Low'],df['Close'],fillna=False).adx().rolling(10).mean()
df['ta_roc'] = ta.momentum.ROCIndicator(df['Close'],fillna=False).roc()

clean_df = indicators.fix_columns_to_float(df)
df_for_ia = clean_df[['Symbol','ta_rsi', 'ta_wr', 'ta_cci', 'ta_macd', '+adx', '-adx', 'adx','adxr_14', 'ta_roc']]

print('hm')
