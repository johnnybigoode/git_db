import mplfinance as mpf
from pathlib import Path
import filecmp

class Drawable():
    
    def __init__( self, symbol, df ) :
        self.symbol = symbol
        self.df = df
        self.style = mpf.make_mpf_style(base_mpf_style='classic', mavcolors=['#1f77b4','#ff7f0e','#2ca02c'])
        self.window = 1
        self.file_extension = '.png'
        self.dpi = 40
        self.last_position_df = df.index.get_loc(df.index.max())
        #self.max_date = str(df.index.max().year)+str(f'{df.index.max().month:02}')+str(f'{df.index.max().day:02}')
        
    def current_series(self):
        return self.df.iloc[self.last_position_df]

    def previous_series(self):
        return self.df.iloc[self.last_position_df - 1]
    
    def max_date(self):
        return str(self.df.iloc[int(self.last_position_df)].name.year)+str(f'{self.df.iloc[int(self.last_position_df)].name.month:02}')+str(f'{self.df.iloc[int(self.last_position_df)].name.day:02}')

    def filename(self,isBackup=False):
        Path(self.symbol).mkdir(exist_ok=True)
        w_str = 'w' + str(self.window)
        rn = str(self.symbol) + '/' + w_str + '-'+ 'dpi'+str(self.dpi) +'-' + str(self.symbol) + '-'+ str(self.last_position_df) + '-' + str(self.max_date()) 
        if(isBackup):
            rn = + '_bak'
        rn = rn + self.file_extension
        return rn

    def file_exists(self):
        my_file = Path(self.filename())
        return my_file.is_file()
            
    def check_signal(self, days):
        r = False
        try:
            this_days_val = self.df.iloc[int(self.last_position_df)].Close
            in_days_val = self.df.iloc[int(self.last_position_df)+days].Close
            if(this_days_val is None): this_days_val = 0
            if(in_days_val is None): in_days_val = 0
            r = float(this_days_val) < float(in_days_val)
        except IndexError as e:
            print(e)
            r = None
        return r

    def plot_to_jpg(self, validation=False, min_date=19900101):
        isTooOld = int(self.max_date()) > int(min_date)
        if(isTooOld or validation or not self.file_exists()):
            apds = [mpf.make_addplot(self.df['MA5'].tail(60),color='#1f77b4'),
                    mpf.make_addplot(self.df['MA10'].tail(60),color='#ff7f0e'),
                    mpf.make_addplot(self.df['MA30'].tail(60),color='#2ca02c'),
                    mpf.make_addplot(self.df['histogram'].tail(60),type='bar',width=0.7,panel=1, color='dimgray',alpha=1,secondary_y=False),
                    mpf.make_addplot(self.df['macd'].tail(60),panel=1,color='b',secondary_y=True),
                    mpf.make_addplot(self.df['signal'].tail(60),panel=1,color='r',secondary_y=True),
                ]
            try:
                if(validation is False):
                    fn = self.filename()
                    mpf.plot(self.df.iloc[self.last_position_df-59:self.last_position_df+1],
                        type='candle',addplot=apds,figscale=1.1,figratio=(8,5),volume_panel=2,panel_ratios=(6,3),
                        axisoff=True,style=self.style, 
                        savefig=dict(dpi=self.dpi, fname=fn,bbox_inches='tight'))
                elif(validation is True):
                    fn = self.filename(isBackup=True)
                    mpf.plot(self.df.iloc[int(self.last_position_df)-59:int(self.last_position_df)+1],
                        type='candle',addplot=apds,figscale=1.1,figratio=(8,5),volume_panel=2,panel_ratios=(6,3),
                        axisoff=True,style=self.style, 
                        savefig=dict(dpi=self.dpi, fname=self.filename(isBackup=True), bbox_inches='tight'))
                    isEqual = filecmp.cmp(fn, self.filename())
                    Path(self.filename(isBackup=True)).unlink()
                    if(isEqual):
                        return True
                    else:
                        return False
                    
            except IndexError as e:
                print(e)
                pass
        self.last_position_df = self.last_position_df - 1