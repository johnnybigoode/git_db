import database
import indicators
import ta

from sklearn.preprocessing import MinMaxScaler
from pandas import DataFrame
import numpy as np 

def organize_data(brute_df):
    db = database.Database()
    empresas_bovespa =  list(brute_df.keys())

    all_df = dict()

    for i in empresas_bovespa:
        try:
            df = brute_df[i]
            df = indicators.fix_columns_to_float(df)
            #the following is a bad hack that will drop prices if they share the same value as the last 5 days across their open/close/high/low
            df = df.drop(df[((df.Open == df.Close.shift(5)) & (df.High.shift(2) == df.Low)) & ((df.Open.shift(3) == df.Close.shift()) & (df.High.shift() == df.Low.shift(4)))].index)
            df['ta_rsi'] = ta.momentum.RSIIndicator(close=df['Close'],fillna=True).rsi()
            df['ta_wr'] = ta.momentum.WilliamsRIndicator(df['High'],df['Low'],df['Close'],fillna=True).wr()
            df['ta_cci'] = ta.trend.CCIIndicator(df['High'],df['Low'],df['Close'],fillna=True).cci()
            df['ta_macd'] = ta.trend.MACD(close=df['Close'],n_slow=15,n_fast=5,n_sign=21,fillna=True).macd()
            df['+adx'] = ta.trend.ADXIndicator(df['High'],df['Low'],df['Close'],fillna=True).adx_pos()
            df['-adx'] = ta.trend.ADXIndicator(df['High'],df['Low'],df['Close'],fillna=True).adx_neg()
            df['adx'] = ta.trend.ADXIndicator(df['High'],df['Low'],df['Close'],fillna=True).adx()
            df['adxr_14'] = ta.trend.ADXIndicator(df['High'],df['Low'],df['Close'],fillna=True).adx().rolling(10).mean()
            df['ta_roc'] = ta.momentum.ROCIndicator(df['Close'],fillna=True).roc()
            df = indicators.fix_columns_to_float(df)

            df['sig1'] = df.Close < df.shift(-1).Close
            df['sig20'] = df.Close < df.shift(-20).Close
            df['sig60'] = df.Close < df.shift(-60).Close
            df['sig90'] = df.Close < df.shift(-90).Close

            all_df[i] = df
        except:
            print('failrure')    
        #print('done '+i)

    the_bogus = []
    the_bogus.append('MGLU3.SA') #yfinance não retorna dados 
    the_bogus.append('MRVE3.SA')
    the_bogus.append('QUAL3.SA')
    the_bogus.append('A1EE34.SA') #error
    the_bogus.append('A1EN34.SA')
    the_bogus.append('A1GI34.SA')
    the_bogus.append('A1GN34.SA')
    the_bogus.append('A1IV34.SA')
    the_bogus.append('A1JG34.SA')
    the_bogus.append('A1ME34.SA')
    the_bogus.append('A1MP34.SA')
    the_bogus.append('A1ON34.SA')
    the_bogus.append('A1OS34.SA')
    the_bogus.append('A1SU34.SA')
    the_bogus.append('A1TM34.SA')
    the_bogus.append('A1VY34.SA')
    the_bogus.append('ABTT34.SA')
    the_bogus.append('ALEF3B.SA')
    the_bogus.append('APTI3.SA')
    the_bogus.append('B1AX34.SA')
    the_bogus.append('B1FC34.SA')
    the_bogus.append('B1LL34.SA')
    the_bogus.append('B1RF34.SA')
    the_bogus.append('BAUH3.SA')
    
    clean_empresas = []
    the_new_bogus = []
    for i in list(brute_df.keys()):
        if(i not in the_bogus):
            clean_empresas.append(i)
        else:
            the_new_bogus.append(i)
    #print(the_new_bogus)

    for i in the_new_bogus:
        if(i in all_df.keys()):
            del all_df[i]

    print('Done')
    return all_df

signals = ['sig1', 'sig20', 'sig60', 'sig90', 'sig20']
#implement method to c heck if signal in list

def clean_data(all_df, signal='sig20'):
    datass = []
    targetss = []
    scaler = MinMaxScaler(feature_range=(0,1))       
    sliding_window = 15
    counter = 0

    for i in all_df.keys():    
        #print(i + ' ', end='')
        counter = counter + 1
        if(counter > 15):
            #print('')
            counter = 0
        try:
            df = all_df[i]
        except KeyError as ke:
            print(i) 
            print('key does not exist', ke)



        #df = df['2011-01-01':'2016-01-01'] #grab data between 2011 and 2016
        df = df[['ta_rsi', 'ta_wr', 'ta_cci', 'ta_macd', '+adx', '-adx', 'adx', 'adxr_14', 'ta_roc', signal]]
        many_tupples = []
        for i in range(0, len(df)-15, sliding_window):
            many_tupples.append((df.iloc[i:i+15][['ta_rsi', 'ta_wr', 'ta_cci', 'ta_macd', '+adx', '-adx', 'adx', 'adxr_14', 'ta_roc']],df.iloc[i][signal]))

        fresh_dataset = DataFrame(many_tupples,columns=['data','target'])

        for v in np.array(fresh_dataset['target'].values):
            targetss.append(v)
        for i in range(0,len(fresh_dataset)):
            scaled_data = scaler.fit_transform(np.array(fresh_dataset['data'].values[i]))
            datass.append(scaled_data)

        #sanity check
        len(targetss) == len(datass)
    return datass, targetss

def cut_test_data(all_df, year_start='2019', year_end='2020',year_tail='2015'):
    test_df = dict()
    if(year_start >= year_end):
        print("you can't have a start year bigger or equal than end year")
        raise ValueError
                    
    for i in all_df.keys():
        test_df[i] = all_df[i].loc[str(year_end)+'-01-01':]    
        all_df[i] = all_df[i].drop(all_df[i].loc[str(year_end)+'-01-01':].index)        
        all_df[i] = all_df[i].drop(all_df[i].loc[str(year_start)+'-01-01':].index)

    return all_df, test_df

def clean_test_data(test_df, signal='sig20'):
    test_datass = []
    test_targetss = []
    scaler = MinMaxScaler(feature_range=(0,1))       
    sliding_window = 15
    counter = 0
    for i in test_df.keys():    
        #print(i + ' ', end='')
        counter = counter + 1
        if(counter > 15):
            #print('')
            counter = 0
        try:
            df = test_df[i]
        except KeyError as ke:
            print(i) 
            print('key does not exist', ke)
        #df = df['2011-01-01':'2016-01-01'] #grab data between 2011 and 2016
        df = df[['ta_rsi', 'ta_wr', 'ta_cci', 'ta_macd', '+adx', '-adx', 'adx', 'adxr_14', 'ta_roc', signal]]
        many_tupples = []
        for i in range(0, len(df)-15, sliding_window):
            many_tupples.append((df.iloc[i:i+15][['ta_rsi', 'ta_wr', 'ta_cci', 'ta_macd', '+adx', '-adx', 'adx', 'adxr_14', 'ta_roc']],df.iloc[i+15][signal]))

        fresh_dataset = DataFrame(many_tupples,columns=['data','target'])

        for v in np.array(fresh_dataset['target'].values):
            test_targetss.append(v)
        for i in range(0,len(fresh_dataset)):
            scaled_data = scaler.fit_transform(np.array(fresh_dataset['data'].values[i]))
            test_datass.append(scaled_data)

    return test_datass, test_targetss

