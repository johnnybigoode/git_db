import database
import indicators
from pathlib import Path
import shutil

import unittest

import drawable
class Test_Graphmaking(unittest.TestCase):
    def setUp(self):
        self.db = database.Database()
        self.df = self.db.load_dataframe_from_sql(limit=120, table_name='A1AP34.SA')
        indicators.add_indicator_to_df(self.df, 'MA', 5)
        indicators.add_indicator_to_df(self.df, 'MA', 10)
        indicators.add_indicator_to_df(self.df, 'MA', 30)
        indicators.add_indicator_to_df(self.df, 'MACD')
        indicators.fix_columns_to_float(self.df)
    
    def test_check_filename(self):
        draw = drawable.Drawable(symbol='A1AP34.SA', df=self.df)
        self.assertEqual(draw.filename(),'A1AP34.SA/w1-dpi40-A1AP34.SA-119-20200803.png')

    def test_draw_write(self):
        draw = drawable.Drawable(symbol='A1AP34.SA', df=self.df)
        f_fname = draw.filename()
        draw.plot_to_jpg()
        my_file = Path(f_fname)
        if my_file.is_file():
            self.assertEqual(0,0)
        else:
            self.assertEqual(0,1)

    def tearDown(self):
         shutil.rmtree("A1AP34.SA/", ignore_errors=False, onerror=None)

    
if __name__ == '__main__':
    unittest.main()