from pathlib import Path

import database
import indicators
import drawable
import re

empresas_mais_5215 = ['BAZA3.SA','TIET3.SA','ITUB3.SA','BBAS3.SA','PETR3.SA']
#empresas_mais_5215 = ['BAZA3.SA','TIET3.SA','ITUB3.SA','BBAS3.SA']

def validate_images():
    p = Path('.')
        
    [x for x in p.iterdir() if x.is_dir()]
    directories = [x for x in p.iterdir() if x.is_dir()]

    for i in directories:
        print(i)
        if ('.SA' in i._str):
            db = database.Database()
            df = db.load_dataframe_from_sql(table_name=i._str)
            indicators.add_indicator_to_df(df, 'MA', 5)
            indicators.add_indicator_to_df(df, 'MA', 10)
            indicators.add_indicator_to_df(df, 'MA', 30)
            indicators.add_indicator_to_df(df, 'MACD')
            indicators.fix_columns_to_float(df)
            draw = drawable.Drawable(symbol=i, df=df)

            dir_name = i._str
            dir_path = Path('./'+dir_name)
            file_paths = [x for x in dir_path.iterdir() if x.is_file()]
            for f in file_paths:
                file_last_position_df = f.name[12:-13]
                draw.last_position_df = file_last_position_df
                print(draw.plot_to_jpg(validation=True))
                print(f)

def move_images():
    p = Path('.')
        
    [x for x in p.iterdir() if x.is_dir()]
    directories = [x for x in p.iterdir() if x.is_dir()]

    for i in directories:
        print(i)
        if ('BAZA3.SA' in i._str):
            db = database.Database()
            df = db.load_dataframe_from_sql(table_name=i._str)
            # indicators.add_indicator_to_df(df, 'MA', 5)
            # indicators.add_indicator_to_df(df, 'MA', 10)
            # indicators.add_indicator_to_df(df, 'MA', 30)
            # indicators.add_indicator_to_df(df, 'MACD')
            # indicators.fix_columns_to_float(df)
            draw = drawable.Drawable(symbol=i, df=df)

            dir_name = i._str
            dir_path = Path('./'+dir_name)
            file_paths = [x for x in dir_path.iterdir() if x.is_file()]
            for f in file_paths:
                file_last_position_df = f.name[18:-13]
                draw.last_position_df = file_last_position_df
                signal = draw.check_signal(1)
                if(signal is not None):
                    move_dir = 'Err'
                    if(signal == True):
                        move_dir = '1'
                    elif(signal == False):
                        move_dir = '0'
                        
                    Path(dir_path).joinpath(move_dir).mkdir(exist_ok=True)
                    move_path = Path(dir_path).joinpath(move_dir).joinpath(f.name)
                    f.rename(move_path)
                else:
                    move_dir = 'OutOfBounds'
                    Path(dir_path).joinpath(move_dir).mkdir(exist_ok=True)
                    move_path = Path(dir_path).joinpath(move_dir).joinpath(f.name)
                    f.rename(move_path)
                print(f)

move_images()