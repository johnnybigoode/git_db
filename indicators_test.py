import database

import unittest

import indicators
class Test_Indicators(unittest.TestCase):
    def setUp(self):
        self.db = database.Database()
        self.df = self.db.load_dataframe_from_sql(limit=120)

    def test_add_ma5_indicator(self):
        indicators.add_indicator_to_df(self.df, 'MA', 5)
        self.assertEqual('MA5' in self.df.columns,True)
        
    def test_add_ma10_indicator(self):
        indicators.add_indicator_to_df(self.df, 'MA', 10)
        self.assertEqual('MA10' in self.df.columns,True)

    def test_add_ma30_indicator(self):
        indicators.add_indicator_to_df(self.df, 'MA', 30)
        self.assertEqual('MA30' in self.df.columns,True)

    def test_add_MACD_indicator(self):
        indicators.add_indicator_to_df(self.df, 'MACD')
        print(self.df.columns)
        self.assertEqual('macd' in self.df.columns,True)

    def test_add_all_indicators(self):
        indicators.add_indicator_to_df(self.df, 'MA', 5)
        indicators.add_indicator_to_df(self.df, 'MA', 10)
        indicators.add_indicator_to_df(self.df, 'MA', 30)
        indicators.add_indicator_to_df(self.df, 'MACD')
        are_indicatores_there = ('MA5' in self.df.columns) and ('MA10' in self.df.columns) and ('MA30' in self.df.columns) and ('macd' in self.df.columns) 
        self.assertEqual(are_indicatores_there,True)

    def test_float_converter(self):
        indicators.fix_columns_to_float(self.df)
        #i mean, checking if will not break
        self.assertEqual(0,0)
        
if __name__ == '__main__':
    unittest.main()