import tensorflow as tf
from tensorflow import keras

from keras.models import Model, Sequential
from keras.layers import Conv1D, Conv2D, MaxPooling2D, MaxPooling1D
from keras.layers import Activation, Dropout, Flatten, Dense, Input
from keras.layers.merge import concatenate

def get_model_3_6_2_funcional(shape=(15, 9)):
    model_in = tf.keras.Input(shape=shape)
    model_conv1d_1 = tf.keras.layers.Conv1D(64, 1)(model_in)
    model_maxpooling1d_1 = tf.keras.layers.MaxPooling1D(pool_size=2)(model_conv1d_1)
    model_conv1d_2 = tf.keras.layers.Conv1D(64, 1)(model_maxpooling1d_1)
    model_maxpooling1d_2 = tf.keras.layers.MaxPooling1D(pool_size=2)(model_conv1d_2)
    model_dropout_1 = tf.keras.layers.Dropout(.2)(model_maxpooling1d_2)
    model_flatten = tf.keras.layers.Flatten()(model_dropout_1)
    model_dense_128 = tf.keras.layers.Dense(128)(model_flatten)
    model_dropout_2 = tf.keras.layers.Dropout(.2)(model_dense_128)
    model_activation = tf.keras.layers.Activation('relu')(model_dropout_2)    
    model_output = tf.keras.layers.Dense(1, activation='sigmoid')(model_activation)
    model = Model(model_in, model_output)
    return model, model_in, model_output

def get_3_7_0():
    input_list = []
    output_list = []
    for i in range(0,3):
        print(i)
        _, input, output = get_model_3_6_2_funcional(shape=(int(15/3), 9))
        input_list.append(input)
        output_list.append(output)
        
    concatenated = concatenate(output_list)
    full_con = Dense(128)(concatenated)
    final_output = Dense(1, activation='sigmoid')(full_con)
    return Model(input_list, final_output)

def get_model_3_6_2():
    model = Sequential()
    model.add(tf.keras.Input(shape=(15, 9)))
    model.add(Conv1D(64, 1))
    model.add(MaxPooling1D(pool_size=2))
    model.add(Conv1D(64, 1))
    model.add(MaxPooling1D(pool_size=2))
    model.add(Dropout(.2))
    model.add(Flatten())
    model.add(Dense(128))
    model.add(Dropout(.2))
    model.add(Activation('relu'))
    model.add(Dense(1, activation='sigmoid'))
    #keras.utils.plot_model(model, show_shapes=True)
    #model.summary()
    return model
