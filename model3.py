#organizes data
import database
import indicators

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib as mpl

import ta

db = database.Database()
empresas_bovespa = ['ABEV3.SA','AZUL4.SA','B3SA3.SA','BBAS3.SA','BBDC3.SA','BBDC4.SA','BBSE3.SA','BEEF3.SA','BPAC11.SA','BRAP4.SA',
                    'BRDT3.SA','BRFS3.SA','BRKM5.SA','BRML3.SA','BTOW3.SA','CCRO3.SA','CIEL3.SA','CMIG4.SA','COGN3.SA','CPFE3.SA',
                    'CRFB3.SA','CSAN3.SA','CSNA3.SA','CVCB3.SA','CYRE3.SA','ECOR3.SA','EGIE3.SA','ELET3.SA','ELET6.SA','EMBR3.SA',
                    'ENBR3.SA','ENGI11.SA','EQTL3.SA','EZTC3.SA','FLRY3.SA','GGBR4.SA','GNDI3.SA','GOAU4.SA','GOLL4.SA','HAPV3.SA',
                    'HGTX3.SA','HYPE3.SA','IGTA3.SA','IRBR3.SA','ITSA4.SA','ITUB4.SA','JBSS3.SA','KLBN11.SA','LAME4.SA','LREN3.SA',
                    #'MGLU3.SA',
                    #'MRFG3.SA','MRVE3.SA','MULT3.SA','NTCO3.SA','PCAR3.SA','PETR3.SA','PETR4.SA','PRIO3.SA','QUAL3.SA',
                    #'RADL3.SA','RAIL3.SA','RENT3.SA','SANB11.SA','SBSP3.SA','SULA11.SA','SUZB3.SA','TAEE11.SA','TIMS3.SA','TOTS3.SA',
                    #'UGPA3.SA','USIM5.SA','VALE3.SA','VIVT4.SA','VVAR3.SA','WEGE3.SA','YDUQ3.SA'
                    ]

all_df = dict()

for i in empresas_bovespa:
    try:
        df = db.load_dataframe_from_sql(table_name=i)
        df = indicators.fix_columns_to_float(df)
        df['ta_rsi'] = ta.momentum.RSIIndicator(close=df['Close'],fillna=False).rsi()
        df['ta_wr'] = ta.momentum.WilliamsRIndicator(df['High'],df['Low'],df['Close'],fillna=False).wr()
        df['ta_cci'] = ta.trend.CCIIndicator(df['High'],df['Low'],df['Close'],fillna=False).cci()
        df['ta_macd'] = ta.trend.MACD(close=df['Close'],n_slow=15,n_fast=5,n_sign=21,fillna=False).macd()
        df['+adx'] = ta.trend.ADXIndicator(df['High'],df['Low'],df['Close'],fillna=False).adx_pos()
        df['-adx'] = ta.trend.ADXIndicator(df['High'],df['Low'],df['Close'],fillna=False).adx_neg()
        df['adx'] = ta.trend.ADXIndicator(df['High'],df['Low'],df['Close'],fillna=False).adx()
        df['adxr_14'] = ta.trend.ADXIndicator(df['High'],df['Low'],df['Close'],fillna=False).adx().rolling(10).mean()
        df['ta_roc'] = ta.momentum.ROCIndicator(df['Close'],fillna=False).roc()
        df = indicators.fix_columns_to_float(df)

        df['sig1'] = df.Close < df.shift(-1).Close
        df['sig20'] = df.Close < df.shift(-20).Close
        df['sig60'] = df.Close < df.shift(-60).Close
        df['sig90'] = df.Close < df.shift(-90).Close

        
        all_df[i] = df
    except:
        print('failrure')    
    print('done '+i)




from pandas import DataFrame

df = all_df['BBAS3.SA']
df = df['2011-01-01':'2016-01-01'] #grab data between 2011 and 2016
df = df[['ta_rsi', 'ta_wr', 'ta_cci', 'ta_macd', '+adx', '-adx', 'adx', 'adxr_14', 'ta_roc', 'sig20']]
many_tupples = []
for i in range(0, len(df)-15, 15):
    many_tupples.append((df.iloc[i:i+15][['ta_rsi', 'ta_wr', 'ta_cci', 'ta_macd', '+adx', '-adx', 'adx', 'adxr_14', 'ta_roc']],df.iloc[i+15]['sig20']))

import numpy as np
import tensorflow as tf

fresh_dataset = DataFrame(many_tupples,columns=['data','target'])

datass = []
targetss = []
for v in np.array(fresh_dataset['target'].values):
    targetss.append(v)
for i in range(0,len(fresh_dataset)):
    
    datass.append(np.array(fresh_dataset['data'].values[i]))

dataset = tf.data.Dataset.from_tensor_slices((datass, targetss))

val_dataframe = fresh_dataset.tail(int(len(fresh_dataset)*0.2))
train_dataframe = fresh_dataset.drop(val_dataframe.index)

print(
    "Using %d samples for training and %d for validation"
    % (len(train_dataframe), len(val_dataframe))
)

print('Done')



partition['train'] = 0
partition['validation'] = 0
#labels