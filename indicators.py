import pandas as pd

def fix_columns_to_float(df):
    df = df.fillna(method='backfill')
    df.index = df.index.astype('datetime64[ns]')
    for i in df.columns:        
        if (i != 'Symbol'):
            df[i] = df[i].replace(to_replace=0, method='ffill')
            df[i] = df[i].astype(float)
    return df
    
def add_label(df):
    windowSize = 11
    counterRow = 0
    numberOfDaysInFile = len(df)
    return_array = []
    while(counterRow <= numberOfDaysInFile):
        print(counterRow, end='')
        counterRow = counterRow + 1
        min_v = 999
        max_v = 0
        if(counterRow > windowSize):
            windowBeginIndex = counterRow - windowSize
            windowEndIndex = windowBeginIndex + windowSize - 1
            windowMiddleIndex = (windowBeginIndex + windowEndIndex)/2
            for i in range(windowBeginIndex, windowEndIndex):
            #for (i = windowBeginIndex;i <= windowEndIndex;i ++):
                number = df.iloc[i]['Close']
                if(number < min_v):
                    min_v = number
                    minIndex = i
                if(number > max_v):
                    max_v = number
                    maxIndex = i
            if(maxIndex == windowMiddleIndex):
                result=-1
            elif(minIndex == windowMiddleIndex):
                result=1
            else:
                result=0
            return_array.append(result)
        else:
            return_array.append(0)

    return_array.pop(0)
    return pd.Series(return_array,name='plr').values
        

    

def add_indicator_to_df(df, ind, *argv ):
    if(ind=="MA"):
        if(len(argv) != 1):
            print('MA indicator requires one and only one value')
            raise Error    
        this_key = str(ind) + str(argv[0])
        print(this_key)
        df[this_key] = df.groupby('Symbol')['Close'].transform(lambda x: x.rolling(int(argv[0]), 1).mean())
        return 
    elif(ind=="DIF"):
        if(len(argv) != 2):
            print('DIF indicator requires two values')
            raise Error    
        first_df = df[argv[0]]
        second_df = df[argv[1]]
        this_key = str(ind) + str(argv[0]) + str(argv[1])
        df[this_key] = first_df - second_df
        return 
    elif(ind=="CROSS"):
        if(len(argv) != 1):
            print('CROSS indicator requires one and only one value')
            raise Error
        cross_key = str(argv[0])
        this_key="CROSS" + str(argv[0])
        df[this_key] = df[cross_key] * df[cross_key].shift() < 0
        return
    elif(ind=="MACD"):
        df['exp12'] = df['Close'].ewm(span=5, adjust=False).mean()
        df['exp26'] = df['Close'].ewm(span=15, adjust=False).mean()
        df['macd'] = df['exp12'] - df['exp26']
        df['signal'] = df['macd'].ewm(span=21, adjust=False).mean()
        df['histogram'] = df['macd'] - df['signal']        
        return 
    else:
        print('fail')