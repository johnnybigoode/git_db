import database
import dataget

import unittest
import sqlalchemy
import pandas

class Test_Database(unittest.TestCase):
    def test_our_engine(self):
        db = database.Database()
        self.assertEqual(type(db.our_engine()), sqlalchemy.engine.base.Engine)

    def test_load_dataframe_from_sql_one_row(self):
        db = database.Database()
        df = db.load_dataframe_from_sql(limit=1)
        self.assertEqual(type(df), pandas.core.frame.DataFrame)

    def test_yfinance(self):
        data = dataget.get_data('BBAS3.SA')
        print(type(data))
        self.assertEqual(0, 0)

    def test_database_update(self):
        result = database.Database.update()        
        self.assertEqual(result, 1)

if __name__ == '__main__':
    unittest.main()