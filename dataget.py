import pandas as pd
import pandas_datareader.data as web
import yfinance as yf
yf.pdr_override()

def get_data(empresa, period = "1y", start = None,end = None):
    if (type(empresa) != list):#if turn into list
        empresa = [empresa]
    
    df_list = []
    this_itr = ''
    for i in empresa:
        if (start is None) and (end is None):
            #this_itr = web.get_data_yahoo(i, period = period)
            this_itr = web.get_data_yahoo(i)
        else:
            this_itr = web.get_data_yahoo(i, start = start,end = end)
        if(this_itr.empty):
            print('is it delisted?  -  '  +i)
        else:
            this_itr['Symbol'] = i
            print('adding '+i)
            df_list.append((i,this_itr))
        
    super_df = pd.DataFrame()
    for i in df_list:
        super_df = super_df.append(i[1])
    return super_df

def get_data_if_not_exists(df_from_sql, i, period):
    #todo not query data that exists?
    return web.get_data_yahoo(i, period = period)
